//
//  GmsMapVCBridge.swift
//  MyFavouritePlaces_SwiftUI
//
//  Created by Mahmoud HodaeeNia on 11/26/21.
//

import Foundation
import SwiftUI
import GoogleMaps

struct GmsMapVCsBridge: UIViewControllerRepresentable {
    @Binding var currentMarkers: [GMSMarker]
    @Binding var currentState: DemoState
    @Binding var currentPosition: CLLocationCoordinate2D

    func makeUIViewController(context: Context) -> MapViewController {
        let vc = MapViewController()
        vc.onCameraPositionChangedDelegate = {
            if self.currentState == .CHOOSING_LOCATION {
                self.currentPosition = $0.target
            }
        }
        return vc
    }

    func updateUIViewController(_ uiViewController: MapViewController, context: Context) {
        print("\(#file): \(#function): \(#line)")
        uiViewController.mapView.clear()
        self.currentMarkers.forEach({ $0.map = uiViewController.mapView })

        uiViewController.imageViewIndicator.isHidden = (self.currentState != .CHOOSING_LOCATION)

        let locations = self.currentMarkers.map({ $0.position })
        if !locations.isEmpty {
            let minOfLats = CLLocationDegrees(locations.compactMap({ $0.latitude }).min()!)
            let maxOfLats = CLLocationDegrees(locations.map({ $0.latitude }).max()!)

            let minOfLongs = CLLocationDegrees(locations.map({ $0.longitude }).min()!)
            let maxOfLongs = CLLocationDegrees(locations.map({ $0.longitude }).max()!)

            let minLocation = CLLocationCoordinate2D(latitude: minOfLats, longitude: minOfLongs)
            let maxLocation = CLLocationCoordinate2D(latitude: maxOfLats, longitude: maxOfLongs)

            let bounds = GMSCoordinateBounds(coordinate: minLocation, coordinate: maxLocation)
            let cameraUpdate: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
            uiViewController.mapView.animate(with: cameraUpdate)
        }
    }
}
