//
//  MapViewController.swift
//  MyFavouritePlaces_SwiftUI
//
//  Created by Mahmoud HodaeeNia on 11/26/21.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController, GMSMapViewDelegate {

    let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
    lazy var mapView = GMSMapView.map(withFrame: .zero, camera: self.camera)

    let imageViewIndicator: UIImageView = UIImageView(image: GMSMarker.markerImage(with: nil))
    var onCameraPositionChangedDelegate: ((GMSCameraPosition) -> Void)? = nil


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mapView.delegate = self

        self.view = self.mapView
        self.view.addSubview(self.imageViewIndicator)

        self.imageViewIndicator.translatesAutoresizingMaskIntoConstraints = false
        let constraintAlignX = NSLayoutConstraint(item: self.imageViewIndicator, attribute: .centerX, relatedBy: .equal, toItem: self.mapView, attribute: .centerX, multiplier: 1, constant: CGFloat.zero)
        let constraintAlignY = NSLayoutConstraint(item: self.imageViewIndicator, attribute: .centerY, relatedBy: .equal, toItem: self.mapView, attribute: .centerY, multiplier: 1, constant: CGFloat.zero)
        self.mapView.addConstraints([constraintAlignX, constraintAlignY])
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.onCameraPositionChangedDelegate?(position)
    }

}
