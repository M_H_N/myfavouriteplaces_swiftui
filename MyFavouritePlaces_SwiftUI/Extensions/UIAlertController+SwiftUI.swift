//
// Created by Mahmoud HodaeeNia on 11/26/21.
//

import Foundation
import SwiftUI
import UIKit

extension UIAlertController {
    convenience init(alert: TextAlert) {
        self.init(title: alert.title, message: alert.message, preferredStyle: .alert)

        addTextField {
            $0.placeholder = alert.placeholder0
            $0.keyboardType = alert.keyboardType
        }
        addTextField {
            $0.placeholder = alert.placeholder1
            $0.keyboardType = alert.keyboardType
        }

        if let cancel = alert.cancel {
            addAction(UIAlertAction(title: cancel, style: .cancel) { _ in
                alert.action(nil, nil)
            })
        }

        if let secondaryActionTitle = alert.secondaryActionTitle {
            addAction(UIAlertAction(title: secondaryActionTitle, style: .default, handler: { _ in
                alert.secondaryAction?()
            }))
        }

        let textField0 = self.textFields?.first
        let textField1 = self.textFields?[1]
        addAction(UIAlertAction(title: alert.accept, style: .default) { _ in
            alert.action(textField0?.text, textField1?.text)
        })
    }
}


extension View {
    public func alert(isPresented: Binding<Bool>, _ alert: TextAlert) -> some View {
        AlertWrapper(isPresented: isPresented, alert: alert, content: self)
    }
}