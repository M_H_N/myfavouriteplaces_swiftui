//
//  InitialView.swift
//  MyFavouritePlaces_SwiftUI
//
//  Created by Mahmoud HodaeeNia on 11/26/21.
//

import SwiftUI
import CoreData
import GoogleMaps

struct InitialView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(entity: Friend.entity(), sortDescriptors: [], animation: .default)
    private var friends: FetchedResults<Friend>

    @State var currentMarkers = [GMSMarker]()

    @State var demoState: DemoState = .WATCHING
    @State var isShowingFriendsList: Bool = false

    @State var selectedLocation: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: 0, longitude: 0)

    var body: some View {
        VStack {
            ZStack(alignment: (self.demoState == .WATCHING ? .bottomTrailing : .topLeading)) {
                // MapViewController
                GmsMapVCsBridge(currentMarkers: $currentMarkers, currentState: $demoState, currentPosition: $selectedLocation)

                if self.demoState == .CHOOSING_LOCATION {
                    Button(role: .cancel, action: {
                        withAnimation {
                            self.demoState = .WATCHING
                        }
                    }, label: { Text("❌") })
                            .frame(alignment: .topLeading)
                            .padding()
                            .background(.gray)
                            .cornerRadius(5)

                } else if self.demoState == .WATCHING {
                    // AddNewPin Button
                    AddButtonView {
                        withAnimation {
                            self.currentMarkers = [GMSMarker]()
                            self.demoState = .CHOOSING_LOCATION
                        }
                    }
                }
            }

            if self.demoState == .WATCHING {
                // Friends list
                ScrollView(.horizontal) {
                    LazyHStack {
                        ForEach(self.friends) { friend in
                            Button("\(friend.first_name ?? "") \(friend.last_name ?? "")") {
                                // Show marks of the friend
                                if let places = (friend.places?.allObjects as? [Place]) {
                                    self.currentMarkers = places.map {
                                        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees($0.latitude), longitude: CLLocationDegrees($0.longitude))
                                        let marker = GMSMarker(position: location)
                                        marker.title = ($0.friends?.allObjects as? [Friend])?
                                                .map({ "\($0.first_name ?? "") \($0.last_name ?? "")" })
                                                .reduce("", { $0 + $1 })
                                        return marker
                                    }
                                }
//
                            }
                                    .padding(3)
                                    .background(.gray)
                                    .cornerRadius(5)

                        }
                    }
                }.frame(height: 50, alignment: .bottom)
                        .background(.ultraThinMaterial)
            } else if self.demoState == .CHOOSING_LOCATION {
                Button("Next") {
                    // Open FriendsList
                    self.isShowingFriendsList = true
                }
                        .frame(maxWidth: .infinity)
                        .padding(3)
                        .background(.gray)
                        .cornerRadius(5)
                        .padding()
            }
        }.sheet(isPresented: $isShowingFriendsList, onDismiss: {
            print("\(#file): \(#function): \(#line) Dismissed")
        }, content: {
            FriendsListView(selectedLocation: self.selectedLocation) {
                withAnimation {
                    print("\(#file): \(#function): \(#line)")
                    self.demoState = .WATCHING
                    self.isShowingFriendsList = false
                }
            }
        })

    }


    struct InitialView_Previews: PreviewProvider {
        static var previews: some View {
            InitialView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
        }
    }
}


/*fileprivate*/ enum DemoState {
    case WATCHING
    case CHOOSING_LOCATION
}
