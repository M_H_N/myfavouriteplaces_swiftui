//
//  FriendsListView.swift
//  MyFavouritePlaces_SwiftUI
//
//  Created by Mahmoud HodaeeNia on 11/26/21.
//

import SwiftUI
import GoogleMaps

struct FriendsListView: View {

    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(entity: Friend.entity(), sortDescriptors: [], animation: .default)
    private var friends: FetchedResults<Friend>

    let selectedLocation: CLLocationCoordinate2D
    let onSaved: () -> Void

    @State private var selectedItems = [Friend]()

    @State private var isAddingNewFriend: Bool = false


    var body: some View {
        VStack {
            List {
                ForEach(self.friends) { friend in
                    HStack {
                        SelectableRow(title: "\(friend.first_name ?? "") \(friend.last_name ?? "")", isSelected: self.selectedItems.contains(friend)) {
                            if self.selectedItems.contains(friend) {
                                self.selectedItems.removeAll(where: { $0 == friend })
                            } else {
                                self.selectedItems.append(friend)
                            }
                        }
                    }
                }.onDelete(perform: self.deleteFriend)
            }

            AddButtonView {
                withAnimation {
                    self.isAddingNewFriend = true
                }
            }

            Button("Save") {
                guard !self.selectedItems.isEmpty else {
                    return
                }

                let newPlace = Place(context: self.viewContext)
                newPlace.longitude = Float(self.selectedLocation.longitude)
                newPlace.latitude = Float(self.selectedLocation.latitude)
                self.selectedItems.forEach({ newPlace.addToFriends($0) })
                try? self.viewContext.save()

                self.onSaved()
            }.buttonStyle(.bordered)
                    .controlSize(.large)
        }.alert(isPresented: $isAddingNewFriend, TextAlert(
                title: "Add New Friend",
                message: "Enter your friend's information",
                placeholder0: "Name",
                placeholder1: "Family",
                accept: "Add",
                cancel: "Cancel",
                keyboardType: UIKeyboardType.default,
                action: { (firstName: String?, lastName: String?) in
                    if let firstName = firstName, let lastName = lastName {
                        withAnimation {
                            let newFriend = Friend(context: self.viewContext)
                            newFriend.first_name = firstName
                            newFriend.last_name = lastName
                            try? self.viewContext.save()
                        }
                    }
                    withAnimation {
                        self.isAddingNewFriend = false
                    }
                },
                secondaryAction: {
                    withAnimation {
                        self.isAddingNewFriend = false
                    }
                }))
    }

    private func deleteFriend(indices: IndexSet) {
        withAnimation {
            indices.map({ self.friends[$0] }).forEach({ self.viewContext.delete($0) })
            try? self.viewContext.save()
        }
    }
}

struct FriendsListView_Previews: PreviewProvider {
    static var previews: some View {
        FriendsListView(selectedLocation: CLLocationCoordinate2D(latitude: 5, longitude: 5)) {
        }
    }
}
