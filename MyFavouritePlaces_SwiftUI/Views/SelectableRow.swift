//
// Created by Mahmoud HodaeeNia on 11/26/21.
//

import Foundation
import SwiftUI

struct SelectableRow: View {
    var title: String
    var isSelected: Bool
    var onAction: () -> Void

    var body: some View {
        Button(action: self.onAction) {
            HStack {
                Text(self.title)
                if self.isSelected {
                    Spacer()
                    Image(systemName: "checkmark")
                }
            }
        }
    }
}