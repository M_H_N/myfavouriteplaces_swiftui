//
//  AppDelegate.swift
//  MyFavouritePlaces_SwiftUI
//
//  Created by Mahmoud HodaeeNia on 11/26/21.
//

import Foundation
import UIKit
import GoogleMaps

class AppDelegate: NSObject, UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil) -> Bool {
        GMSServices.provideAPIKey("AIzaSyAAqqsNjVlI0NmwaOOwFNhrl965nd_jrQE")
        return true
    }
}
