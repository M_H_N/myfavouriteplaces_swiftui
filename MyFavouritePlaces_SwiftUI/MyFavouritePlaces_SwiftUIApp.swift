//
//  MyFavouritePlaces_SwiftUIApp.swift
//  MyFavouritePlaces_SwiftUI
//
//  Created by Mahmoud HodaeeNia on 11/26/21.
//

import SwiftUI

@main
struct MyFavouritePlaces_SwiftUIApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
//            ContentView()
//                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
            InitialView()
                    .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
